package stellar.web;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.validation.BindingResult;
import stellar.data.SystemRepository;
import stellar.model.PlanetarySystem;


import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
class StellarControllerTest {

    @InjectMocks
    StellarController controller;

    @Mock
    SystemRepository systemRepository;

    @Mock
    BindingResult bindingResult;

    @Test
    void testAddSystemSuccess() {
        Mockito.when(systemRepository.save(Mockito.any(PlanetarySystem.class))).thenReturn(new PlanetarySystem());
        Mockito.when(bindingResult.hasErrors()).thenReturn(false);

        PlanetarySystem system = new PlanetarySystem();
        system.setName("Fake System");
        system.setStar("Fake");
        system.setDistance(1);
        String viewName = controller.addSystem(system, bindingResult);
        Assert.assertEquals("redirect:/web/systems", viewName);
    }

    @Test
    void testAddSystemFailure() {

        Mockito.when(bindingResult.hasErrors()).thenReturn(true);

        PlanetarySystem system = new PlanetarySystem();
        system.setName("Fake System");
        system.setStar("Fake");
        system.setDistance(1);
        String viewName = controller.addSystem(system, bindingResult);
        Assert.assertEquals("addSystem", viewName);
    }


}