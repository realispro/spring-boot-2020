package stellar.data;

import org.springframework.data.jpa.repository.JpaRepository;
import stellar.model.PlanetarySystem;

import java.util.List;

public interface SystemRepository extends JpaRepository<PlanetarySystem, Integer> {

    List<PlanetarySystem> findByNameLike(String like);
}
