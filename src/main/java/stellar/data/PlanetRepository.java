package stellar.data;

import org.springframework.data.jpa.repository.JpaRepository;
import stellar.model.Planet;
import stellar.model.PlanetarySystem;

import java.util.List;

public interface PlanetRepository extends JpaRepository<Planet, Integer> {

    List<Planet> findBySystem(PlanetarySystem system);
}
