package stellar.soap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import stellar.data.SystemRepository;
import stellar.soap.dto.GetSystemRequest;
import stellar.soap.dto.GetSystemResponse;
import stellar.soap.dto.PlanetarySystem;

@Endpoint
public class SystemsEndpoint {

    @Autowired
    SystemRepository systemRepository;

    @PayloadRoot(namespace = "http://www.stellar/soap/dto", localPart = "getSystemRequest")
    @ResponsePayload
    public GetSystemResponse getCountry(@RequestPayload GetSystemRequest request) {
        GetSystemResponse response = new GetSystemResponse();

        stellar.model.PlanetarySystem entity = systemRepository.findById(request.getId()).orElseThrow();

        PlanetarySystem dto = new PlanetarySystem();
        dto.setName(entity.getName());
        dto.setDistance(entity.getDistance());
        dto.setStar(entity.getStar());

        response.setSystem(dto);

        return response;
    }
}
