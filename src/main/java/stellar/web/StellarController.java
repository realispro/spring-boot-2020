package stellar.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import stellar.data.PlanetRepository;
import stellar.data.SystemRepository;
import stellar.model.PlanetarySystem;

import java.util.List;

@Controller
@RequestMapping("/web")
public class StellarController {

    @Autowired
    PlanetRepository planetRepository;

    @Autowired
    SystemRepository systemRepository;


    @Autowired
    private SystemValidator validator;

    @InitBinder
    void initBinding(WebDataBinder binder){
        binder.setValidator(validator);
    }


    @GetMapping("/systems")
    public String getSystems(Model model, @RequestParam(value = "phrase", required = false) String phrase){
        model.addAttribute(
                "systems",
                phrase==null || phrase.trim().isEmpty()? systemRepository.findAll() : systemRepository.findByNameLike(phrase));
        return "systems";
    }

    @GetMapping("/planets")
    public String getPlanetsBySystem(Model model, @RequestParam(value="systemId") int systemId){
        model.addAttribute("planets", planetRepository.findBySystem(systemRepository.findById(systemId).orElse(null)));
        return "planets";
    }

    @GetMapping("/addSystem")
    public String prepareAddSystem(Model model){
        model.addAttribute("systemForm", new PlanetarySystem());
        return "addSystem";
    }

    @PostMapping("/addSystem")
    public String addSystem(@ModelAttribute("systemForm") @Validated PlanetarySystem system, BindingResult br){

        if(br.hasErrors()){
            return "addSystem";
        }

        systemRepository.save(system);
        return "redirect:/web/systems";
    }

}
